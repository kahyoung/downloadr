var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function (user, done) {
  done(null, user.username);
});

passport.deserializeUser(function (username, done) {
  User.findOne({username: username}, function (err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function (username, password, done) {

    User.findOne({username: username}, function (err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false, {message: 'Incorrect username.'});
      }

      Bcrypt.compare(password, user.password, function (err, res) {

        // If no response, then the password was wrong
        if (!res) {
          return done(null, false, {
            message: 'Invalid Password'
          });
        }

        var returnUser = {
          username: user.username,
          createdAt: user.createdAt,
          id: user.id
        };

        // Return the logged in user as success
        return done(null, returnUser);
      });

    });
  }
));
