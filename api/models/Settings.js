/**
 * Settings.js
 *
 * @description :: Global Settings for the Downloadr application
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    path: {
      type: 'string',
      defaultsTo: '/tmp/webtorrent'
    },
    toJSON: function() {
      var obj = this.toObject();

      delete obj.id;

      return obj;
    }
  },
  autoCreatedAt: false
};

