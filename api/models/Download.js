/**
* Download.js
*
* @description :: Basic Download Schema - The meta information about a downloaded file
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    link: {
      type: "string",
      required: true,
      unique: true,
      primaryKey: true
    },
    toJSON: function() {
      var obj = this.toObject();

      /**
       * We should get extra information about the Torrent and relay that to the user.
       *
       * This properties include:
       *  - The name of the Download
       *  - The progress of the Download
       */
      var torrent = sails.webTorrent.client.get(obj.link);

      // Torrent can be null if WebTorrent is not currently downloading the file
      if (torrent) {

        // Add Name property
        obj.name = torrent.name;

        // Add Progress property
        obj.progress = torrent.progress;
      }

      return obj;
    }
  }
};

