/**
* User.js
*
* @description :: Users of the application - also contains User specific settings
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true,
      primaryKey: true
    },
    password: {
      type: 'string',
      required: true
    },

    // We want to hide the password when returning the user
    toJSON: function() {
      var obj = this.toObject();

      delete obj.password;

      return obj;
    }
  },

  // Before creating the user, we want to encrypt the password
  beforeCreate: function(user, cb) {

    // Let's encrypt the user's password
    Bcrypt.encrypt(user.password, function(err, result) {
      if (err) {
        return cb(err);
      } else {
        user.password = result;
        cb(null, user);
      }
    });
  }
};

