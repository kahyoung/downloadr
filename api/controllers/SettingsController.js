/**
 * SettingsController
 *
 * @description :: Server-side logic for managing settings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // When doing a GET on the collection, always just return the single settings
  find: SettingsService.get,

  // Call the Settings Service update function to update both the JS and the DB settings
  update: SettingsService.update,

  /**
   * Unsupported methods
   */

  findOne: function(req, res) {
    res.notFound();
  },

  create: function(req, res) {
    res.methodNotAllowed();
  },

  destroy: function(req, res) {
    res.methodNotAllowed();
  }
};

