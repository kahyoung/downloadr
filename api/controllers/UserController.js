/**
 * UserController
 *
 * @description :: Server-side logic for managing users - includes authentication
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');
module.exports = {

  login: function(req, res) {
    passport.authenticate('local', function(err, user, info) {
      if ((err) || (!user)) {
        return res.send(401, {
          message: info.message,
          user: user
        });
      }
      req.logIn(user, function(err) {
        if (err) {
          return res.send(401, err.message);
        }

        return res.redirect('/');
      });

    })(req, res);
  },

  logout: function(req, res) {
    req.logout();
    res.redirect('/');
  },

  /**
   * Unsupported methods
   */

  find: function(req, res) {
    res.methodNotAllowed();
  },

  findOne: function(req, res) {
    res.methodNotAllowed();
  }
};

