/**
 * SettingsService
 *
 * @description :: This service contains the settings and all helper methods related to settings
 */

module.exports = {

  /**
   * This method will check if global settings already exists
   *
   * If they do exist, then we set the JS object of settings to the stored values
   *
   * If they don't exist, then we create a new settings object, and set that to the JS object
   * @param cb
   */
  init: function(cb) {
    sails.downloadr = sails.downloadr || {};

    Settings
      .find({})
      .exec(function(err, settings) {
        if (err) {
          return cb(err);
        }

        // If there are no settings, then we need to create them
        if (_.isEmpty(settings)) {
          Settings
            .create({})
            .exec(function(err, settings) {
              if (err) {
                return cb(err);
              }

              sails.downloadr.settings = settings;

              cb();
            });
        } else {
          // If we have settings, then get the first settings object
          sails.downloadr.settings = settings[0];

          cb();
        }
      });
  },

  /**
   * Returns the in-memory settings as the response
   */
  get: function(req, res) {
    res.send(sails.downloadr.settings.toJSON());
  },

  /**
   * Updates the settings in both the DB and in memory
   */
  update: function(req, res) {
    Settings
      .update(sails.downloadr.settings, req.allParams(), function(err, updated) {
        if (err) {
          return res.send(500, err);
        }

        if (_.isEmpty(updated)) {
          return res.badRequest();
        }

        sails.downloadr.settings = updated[0];

        return res.send(updated[0].toJSON());
      });
  }
};
