/**
 * Bcrypt
 *
 * @description :: This service contains all helper methods related to the Bcrypt package
 */

var bcrypt = require('bcrypt');

module.exports = {

  /**
   * Compare
   *
   * Compares the two strings to determine if same
   *
   * @param a - The first string to compare
   * @param b - The second string to compare
   * @param cb - A function with params (err, response)
   */
  compare: function(a, b,  cb) {
    bcrypt.compare(a, b, cb);
  },

  /**
   * Encrypt
   *
   * Encrypts the given string, and returns the result using the given call back
   *
   * @param string - The string to be encrypted
   * @param cb - A function with params (err, result)
   */
  encrypt: function(string, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(string, salt, cb);
    });
  }
};
