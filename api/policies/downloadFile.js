/**
 * downloadFile
 *
 * @module      :: Policy
 * @description :: This policy will go and download the actual torrent file before creating the row in the DB
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

var parseTorrent = require('parse-torrent');

module.exports = function(req, res, next) {

  var link = req.param('link');

  try {
    // First thing we need to do is parse the link to make sure that it is valid
    parseTorrent(link);

    // Once we've parsed the link, then we can actually add the torrent
    sails.webTorrent.client.add(link, {path: sails.downloadr.settings.path}, function() {
      return next();
    });
  } catch (error) {
    // If any error happens (most likely the link is invalid), throw a bad request
    return res.badRequest(error.toString());
  }
};
