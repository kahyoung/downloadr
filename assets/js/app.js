angular
  .module('downloadr', ['focusOn', 'downloadr.controllers'])

/**
 * This authInterceptor will check any rejected http request
 *
 * If the rejection status is Unauthorized, then we should redirect to the home page, as it will
 * now show a login screen
 */
  .factory('authInterceptor', ['$q', function($q) {
    return {
      responseError: function(rejection) {
        // List of Statuses which should cause a redirect
        var redirectStatuses = [401];

        // List of URLs which can be ignored
        var ignorableUrls = '/api/login';

        // If we've gotten an Unauthorized request, then redirect back to home - it'll now show the login screen
        if (!rejection.config.url.endsWith(ignorableUrls) && _.contains(redirectStatuses, rejection.status)) {
          window.location.href = '/';
        }

        return $q.reject(rejection);
      }
    };
  }])
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  }]);
