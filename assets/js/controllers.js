angular
  .module('downloadr.controllers', [])

  .controller('LoginCtrl', ['$scope', '$http', 'focus', function($scope, $http, focus) {
    $scope.user = {};
    $scope.isSaving = false;
    $scope.response = '';

    $scope.login = function() {
      if ($scope.isSaving) {
        return;
      }

      $scope.isSaving = true;
      $scope.response = '';

      $http
        .post(window.location.origin + '/api/login', $scope.user)
        .then(function() {
          window.location.href = '/';
        })
        .catch(function() {
          $scope.response = 'Unable to Log In';
          $scope.isSaving = false;
          focus('username');
        })
    };
  }])

  .controller('NavCtrl', ['$scope', '$http', 'focus', function($scope, $http, focus) {
    $scope.settingsShown = false;
    $scope.isSaving = false;
    $scope.response = '';

    $scope.settings = {
      path: null
    };

    $scope.toggleSettings = function(show) {
      if (show) {
        $scope.settings.path = null;
        $scope.response = '';
        focus('path');
      }

      $scope.settingsShown = show;
    };

    $scope.saveSettings = function() {
      if ($scope.isSaving) {
        return;
      }

      $scope.isSaving = true;

      $http
        .put(window.location.origin + '/api/settings', $scope.settings)
        .then(function() {
          $scope.toggleSettings(false);
        })
        .catch(function() {
          $scope.isSaving = false;
          $scope.response = 'Unable to Save Settings';
          focus('path');
        });
    };
  }])

/**
 * This controller is a basic controller that will handle the POSTing to start downloading a file
 */
  .controller('DownloadCtrl', ['$scope', '$http', 'focus', function($scope, $http, focus) {
    $scope.location = "";
    $scope.isSaving = false;
    $scope.response = '';

    /**
     * This function will do the actual POSTing
     */
    $scope.download = function() {
      if ($scope.isSaving) {
        return;
      }

      $scope.isSaving = true;
      $scope.response = '';

      $http.post(window.location.origin + '/api/downloads', {link: $scope.location})
        .then(function() {
          $scope.location = '';
          $scope.response = 'Downloading now';
        })
        .catch(function() {
          $scope.response = 'Failed to start Download';
        })
        .finally(function() {
          $scope.isSaving = false;
          focus('link');
        });
    };
  }]);
